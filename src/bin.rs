#[macro_use]
extern crate log;

use std::{ptr::read, time::Duration};

use hid;

use hyperx_cloud::HyperXCloud;

fn battery_percentage(charger: u8, magic: u8) -> u8 {
    let percentage = match charger {
        0x10 => 100,
        0x11 => {
            info!("charged");
            100
        }
        0xf => match magic {
            130_u8..=u8::MAX => 100,
            120..=129 => 95,
            100..=119 => 90,
            70..=99 => 85,
            50..=69 => 80,
            20..=49 => 75,
            0..=19 => 70,
        },
        0xe => match magic {
            251..=u8::MAX => {
                warn!("unexpected value in magic");
                0
            }
            240..=250 => 65,
            220..=239 => 60,
            208..=219 => 55,
            200..=207 => 50,
            190..=199 => 45,
            180..=189 => 40,
            169..=179 => 35,
            159..=168 => 30,
            148..=158 => 25,
            119..=147 => 20,
            90..=118 => 15,
            0..=89 => 10,
        },
        _ => {
            warn!("unknow charger state: 0x{:x}", charger);
            0
        }
    };

    percentage
}

fn main() {
    std::env::set_var("RUST_LOG", "debug");
    std::env::set_var("RUST_BACKTRACE", "1");

    env_logger::init();

    info!("starting up");
    //let hid = hid::init().unwrap();

    let mut hyper_x = HyperXCloud::new(30);
    loop {
        let event = hyper_x.listen();
        info!("got event: {:?}", event);
        //std::thread::sleep(Duration::from_secs(2));
    }
    /*for device in hid.find(Some(0x0951), Some(0x1723)) {
        info!("path {} ", device.path().to_str().unwrap());
        info!("ID {:x}:{:x} ", device.vendor_id(), device.product_id());

        if let Some(name) = device.manufacturer_string() {
            info!("manufactoure {} ", name);
        }

        if let Some(name) = device.product_string() {
            info!("product {} ", name);
        }

        if let Some(name) = device.serial_number() {
            info!("serial {} ", name);
        }

        //info!("path {:?} ", device.path());
        info!("usage_page: {:?}", device.usage_page());
        info!("interface_number: {:?}", device.interface_number());
        info!("usage: {:?}", device.usage());

        //info!();
        println!();
        let mut device = device.open().unwrap();
        device.blocking(true).unwrap();
        loop {
            let mut buffer: [u8; 20] = [0; 20];
            if let Some(readed) = device
                .data()
                .read(&mut buffer, Duration::from_secs(20))
                .unwrap()
            {
                //info!("readed: {:?}, buffer: {:x?}", readed, buffer);
                match readed {
                    2 => {
                        //mute & on & off
                        match (buffer[0], buffer[1]) {
                            (0x64, 0x03) => info!("power off"),
                            (0x64, 0x01) => info!("power on"),
                            (0x65, muted) => {
                                if muted == 0x04 {
                                    info!("muted");
                                } else {
                                    info!("unmuted");
                                }
                            }
                            (a, b) => warn!("unhandled len 2 packet: [{}, {}]", a, b),
                        };
                    }
                    5 => {
                        //volume
                        match (buffer[0], buffer[1]) {
                            (0x01, 0x01) => info!("volum up"),
                            (0x01, 0x02) => info!("volum down"),
                            (0x01, 0x00) => {}
                            (a, b) => warn!("unhandled len 5 packet: [{}, {}]", a, b),
                        }
                    }
                    20 => {
                        //charging
                        let chargeState = buffer[3]; //17
                                                     //info!("3: {}4: {x}, ")
                        let magic = buffer[4]; //68

                        info!("chargerState: {:x}, magic: {:x}", chargeState, magic);
                        let percentage = battery_percentage(chargeState, magic);
                        info!("charger percentage: {}", percentage);
                    }
                    _ => warn!("unknow packet lenght: {}", readed),
                }
            } else {
                let batter_req = [0x21u8, 0xff, 0x05];
                device.data().write(batter_req).unwrap();
            }
        }
    }*/
    /*let hid_hyperx =  hid.devices().find(Some(0x0951), Some(0x1723)).unwrap();
    //println!("{:?}", hid_hyperx);
    //println!("found devices: {}", hid_hyperx.count());
    //let hid_hyperx = hid_hyperx.last().unwrap();
    //println!("found devices: {:?}", hid_hyperx.vendor_id());
    info!("path: {:?}", hid_hyperx.path());
    info!("usage_page: {:?}", hid_hyperx.usage_page());
    info!("interface_number: {:?}", hid_hyperx.interface_number());
    info!("usage: {:?}", hid_hyperx.usage());

    let hid_hyperx = hid_hyperx.open().unwrap();
    info!("exit");*/
}
