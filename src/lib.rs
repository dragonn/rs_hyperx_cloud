#[macro_use]
extern crate log;

use serde_derive::{Deserialize, Serialize};

use std::iter::Inspect;
use std::thread::sleep;
use std::time::{Duration, Instant};

use hid;

const hyperx_pidvid: [(u16, u16); 1] = [
    (0x0951, 0x1723), //HyperX Cloud Flight
];

#[derive(Debug, Deserialize, Serialize, Clone, PartialEq, Eq)]
pub enum Volume {
    Up,
    Down,
}

#[derive(Debug, Deserialize, Serialize, Clone, PartialEq, Eq)]
pub enum Mute {
    Muted,
    Unmuted,
}

#[derive(Debug, Deserialize, Serialize, Clone, PartialEq, Eq)]
pub enum Power {
    On,
    Off,
}

#[derive(Debug, Deserialize, Serialize, Clone, PartialEq, Eq)]
pub enum Event {
    Connected,
    Disconnected,
    Power(Power),
    Volume(Volume),
    Mute(Mute),
    Battery(u8),
    Unknow,
    Ignore,
}

pub struct HyperXCloud {
    manager: &'static hid::Manager,
    device: Option<hid::Handle>,
    battery_refresh: Duration,
    last_event: Event,
    last_event_time: Instant,
    last_event_ignore: u8,
}

impl HyperXCloud {
    pub fn new(battery_refresh_secs: u64) -> Self {
        //przerobić to na pobieranie managera z zewnatrz
        let manager = Box::leak(Box::new(hid::init().unwrap()));
        Self {
            manager,
            device: None,
            battery_refresh: Duration::from_secs(battery_refresh_secs),
            last_event: Event::Ignore,
            last_event_time: Instant::now(),
            last_event_ignore: 0,
        }
    }

    fn battery_percentage(charger: u8, magic: u8) -> u8 {
        let percentage = match charger {
            //charging?
            0x10 => 100,
            0x11 => {
                info!("charged");
                100
            }
            0xf => match magic {
                130_u8..=u8::MAX => 100,
                120..=129 => 95,
                100..=119 => 90,
                70..=99 => 85,
                50..=69 => 80,
                20..=49 => 75,
                0..=19 => 70,
            },
            0xe => match magic {
                251..=u8::MAX => {
                    warn!("unexpected value in magic");
                    0
                }
                240..=250 => 65,
                220..=239 => 60,
                208..=219 => 55,
                200..=207 => 50,
                190..=199 => 45,
                180..=189 => 40,
                169..=179 => 35,
                159..=168 => 30,
                148..=158 => 25,
                119..=147 => 20,
                90..=118 => 15,
                0..=89 => 10,
            },
            //0xd missing, not charging
            0xd => match magic {
                251..=u8::MAX => {
                    warn!("unexpected value in magic");
                    0
                }
                240..=250 => 10,
                220..=239 => 9,
                208..=219 => 8,
                200..=207 => 7,
                190..=199 => 6,
                180..=189 => 5,
                169..=179 => 4,
                159..=168 => 3,
                148..=158 => 2,
                119..=147 => 1,
                90..=118 => 1,
                0..=89 => 1,
            },
            //0xc is probaly under 10 % maybe 0%?
            0xc => {
                //warn!("zero charge reached");
                0
            }
            _ => {
                warn!("unknow charger state: 0x{:x} magic: 0x{:x}", charger, magic);
                0
            }
        };

        percentage
    }

    fn decode(buffer: &[u8; 20], count: usize) -> Option<Event> {
        match count {
            2 => {
                //mute & on & off
                match (buffer[0], buffer[1]) {
                    (0x64, 0x03) => Some(Event::Power(Power::Off)),
                    (0x64, 0x01) => Some(Event::Power(Power::On)),
                    (0x65, muted) => {
                        if muted == 0x04 {
                            Some(Event::Mute(Mute::Muted))
                        } else {
                            Some(Event::Mute(Mute::Unmuted))
                        }
                    }
                    (a, b) => {
                        warn!("unhandled len 2 packet: [{}, {}]", a, b);
                        Some(Event::Unknow)
                    }
                }
            }
            5 => {
                //volume
                match (buffer[0], buffer[1]) {
                    (0x01, 0x01) => Some(Event::Volume(Volume::Up)),
                    (0x01, 0x02) => Some(Event::Volume(Volume::Down)),
                    (0x01, 0x00) => None,
                    (a, b) => {
                        warn!("unhandled len 5 packet: [{}, {}]", a, b);
                        Some(Event::Unknow)
                    }
                }
            }
            20 => {
                //charging
                let chargeState = buffer[3]; //17
                                             //info!("3: {}4: {x}, ")
                let magic = buffer[4]; //68

                //info!("chargerState: 0x{:x}, magic: 0x{:x}", chargeState, magic);
                let percentage = Self::battery_percentage(chargeState, magic);
                //info!("charger percentage: {}", percentage);
                Some(Event::Battery(percentage))
            }
            _ => {
                warn!("unknow packet lenght: {}", count);
                Some(Event::Unknow)
            }
        }
    }

    pub fn debounce(&mut self, mut event: Event) -> Event {
        let debounced_event = if event != self.last_event {
            let new_event = if event == Event::Mute(Mute::Unmuted) {
                if self.last_event_time.elapsed().as_millis() < 200 && self.last_event == Event::Power(Power::On) {
                    Event::Ignore
                } else {
                    event
                }
            } else {
                event
            };
            self.last_event_ignore = 0;
            self.last_event = new_event.clone();
            new_event
        } else {
            match event {
                Event::Volume(_) => {
                    if self.last_event_time.elapsed().as_millis() < 100 && self.last_event_ignore < 4 {
                        Event::Ignore
                    } else {
                        event
                    }
                }
                _ => event,
            }
        };

        if debounced_event == Event::Ignore {
            if self.last_event_ignore >= 4 {
                self.last_event_ignore = 0;
            }
            self.last_event_ignore += 1;
        } else {
            self.last_event_ignore = 0;
        }

        self.last_event_time = Instant::now();
        debounced_event
    }

    pub fn listen(&mut self) -> Event {
        let new_event;
        match &mut self.device {
            Some(ref mut device) => {
                let mut buffer: [u8; 20] = [0; 20];
                loop {
                    if let Ok(readed) = device.data().read(&mut buffer, self.battery_refresh) {
                        if let Some(count) = readed {
                            //info!("readed: {:?}, buffer: {:x?}", readed, buffer);
                            if let Some(event) = Self::decode(&buffer, count) {
                                new_event = event;
                                break;
                            }
                        } else {
                            let batter_req = [0x21u8, 0xff, 0x05];
                            if let Err(_) = device.data().write(batter_req) {
                                new_event = Event::Disconnected;
                                break;
                            }
                        }
                    } else {
                        self.device = None;
                        new_event = Event::Disconnected;
                        break;
                    }
                }
            }
            None => {
                info!("searching for connected hyperx cloud");
                loop {
                    let device = hyperx_pidvid.iter().find_map(|(pid, vid)| self.manager.find(Some(*pid), Some(*vid)).into_iter().last());
                    if let Some(device) = device {
                        if let Ok(device) = device.open() {
                            self.device = Some(device);
                            new_event = Event::Connected;
                            break;
                        }
                    }
                    sleep(Duration::from_millis(100));
                }
            }
        };

        self.debounce(new_event)
    }
}

impl Drop for HyperXCloud {
    fn drop(&mut self) {
        //this is only for making sure hid::Manager is droped, not leaked since whey are doing Box::leak on it
        unsafe {
            let manager_point: *const hid::Manager = self.manager;
            let manager_drop = Box::from_raw(manager_point as *mut hid::Manager);
        }
    }
}
